from setuptools import setup

setup(
    name="redmine-cli",
    version="0.1.0.0",
    py_modules = ["redm"],
    install_requires = [
        "click",
        "python-redmine",
        "colorama",
        "profig",
        "appdirs",
    ],
    entry_points = """
        [console_scripts]
        redm=redm:cli
    """,
)
